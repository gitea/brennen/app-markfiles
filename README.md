marks - mark and operate on files
=================================

**This is a rough proof of concept.  It probably won't eat your files, but I
make no promises whatsoever.**

synopsis
--------

<!-- exec -->

    $ marks --help
    /usr/local/bin/marks - mark and operate on files
    
    Usage: marks [command] [args]
        marks               - List current marks (shortcut for mark ls)
        marks add [path]    - Add a file path to the mark list
        marks + [path]      - Shorthand for add
        marks remove [path] - Remove a file path from the mark list
        marks - [path]      - Shorthand for remove
        marks clear         - Clear mark list
        marks cp            - Copy marked files to current directory
        marks each [cmd]    - Execute command for each marked file
        marks ls            - List current marks
        marks mv            - Move files to current directory and unmark
        marks -h            - Print this help message

<!-- end -->

example
-------

```
# Some files to work with:
brennen@inertia:~/Downloads $ ls
binti.epub  rogue_protocol.epub  unix_reader.pdf

# Mark a couple of them:
brennen@inertia:~/Downloads $ mark + *.epub
brennen@inertia:~/Downloads $ marks
/home/brennen/Downloads/rogue_protocol.epub
/home/brennen/Downloads/binti.epub

# Jump to a target directory:
brennen@inertia:~/Downloads $ cd ~/books/sf

# Move the files here:
brennen@inertia:~/books/sf $ marks mv
Moved: /home/brennen/Downloads/rogue_protocol.epub
Moved: /home/brennen/Downloads/binti.epub

# Note that the mark list is now empty:
brennen@inertia:~/books/sf $ marks
brennen@inertia:~/books/sf $
```

installing
----------

I haven't uploaded a recent version of this to CPAN yet.  For now,
I expect this would work on a Debian, at any rate:

```sh
# Module::Build may be installed, but if not, either:
sudo apt install libmodule-build-perl

# Or:
sudo cpan -i Module::Build

# Then:
git clone https://code.p1k3.com/gitea/brennen/app-markfiles.git
cd app-markfiles
perl Build.PL
./Build installdeps
./Build test
sudo ./Build install
```

rationale, notes
----------------

This is a proof of concept and the interface might change.  Right now it's in
poorly-organized Perl and shell, and has some dependencies.  It's pretty tiny
and maybe I'll polish it up or port it to some language that squicks the cool
kids out less, but also I might not if it suits my needs well enough.  Right
now it's in a state where it feels usable on my machine, so I think you could
probably try it, but there are definitely bugs.

In the very simple case this is a worse interface than just typing `mv
somefiles ~/some/path`.  All the same, I find it pretty handy to stash the path
of one or more files somewhere while I figure out what I'm going to do with
them, and it permits building up a list while hopping around the filesystem
working on other things.

There are definitely bugs in this implementation.

This is meant to enable unixy one-tool-one-job patterns (thanks to Joey Hess
for the suggestion):

```sh
marks + *.foo
mv $(marks) dest
```

...but it also includes convenience cruft like `marks cp`, `marks mv`, and
`marks each`.  My thinking is that it's nice to be able to do operations that
clear files from the list as they're manipulated, and have a few extra built-in
guard rails.

I'm not totally sure how interactive this should be / how much handholding I
want.

TODO
----

  - [ ] sqlite - too heavyweight?
  - [ ] sqlite - version the schema, if there's a good reason to keep it
  - [ ] handle `~` expansion (and general shell globbing?) in files/stdin?
  - [ ] more graceful installation instrux
  - [ ] `remove` feels like it's going to get confused with an operation that
        deletes the file rather than removing the mark from the list.  Should
        this be called `unmark` or something instead?
  - [ ] it'd be cool to plumb this into fzf a little bit
